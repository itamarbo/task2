<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<h1>This is your task list</h1>

@if (Request::is('tasks')) 
<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
    @else
<h2><a href="{{action('TaskController@index')}}">show all tasks</a></h2>    
@endif

<ul>
    @foreach($tasks as $task)
    <li>
         id: {{$task->id}} title:{{$task->title}} 
         <a href= "{{route('tasks.edit', $task->id )}}"> Edit this task </a>
    
    @can('manager')

       @if ($task->status)
           <button id ="" value="1"> Done!</button>
         @else
           <button style="text-decoration: underline" id ="{{$task->id}}" value="0"> Mark as done</button>
       @endif

    <form method = 'post' action="{{action('TaskController@destroy', $task->id)}}">
    @csrf
    @method('DELETE')
        <div class = "form-group">
            <input type ="submit" class = "form-control" name="submit" value ="Delete task">
        </div>

    </form>
    @endcan
    </li>
    @endforeach
</ul>

<a href="{{route('tasks.create')}}">Create a new task </a>

<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
</script>



